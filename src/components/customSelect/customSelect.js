import React, {Component, Fragment} from 'react'
import './customSelect.scss'

export default class CustomSelect extends Component {

    state = {
        isOpen: false,
        options: [
            {
                value: 'tesV1',
                title: 'qweqwe'
            },
            {
                value: 'tesV2',
                title: 'qweqxcas'
            },
            {
                value: 'tesV3',
                title: 'zxcvqwddeqwe'
            },
        ],
        selected: null,
        defaultValue: 'Укажите значение'
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.items !== this.props.items){
            this.setState(() => {
                return {
                    options: this.props.items
                }
            })
        }
    }

    componentDidMount() {
        const { items } = this.props

        this.setState(({ state }) => {
            return {
                options: items
            }
        })
    }

    toggleSelect(visible){
        this.setState(({isOpen}) => {
            return {
                isOpen: visible !== undefined ? visible : !isOpen
            }
        })
    }

    onSelect = (e, index) => {
        const { options } = this.state

        this.setState(({selected}) => {
            return {
                selected: options[index]
            }
        }, () => {
            this.toggleSelect(false)

            this.props.eventChange(options[index])
            console.log(this.state.selected.value);
        })
    }

    render() {

        const { isOpen, options, selected, defaultValue } = this.state

        const optionsList = options.map((item, i) => {

            const { value, title } = item

            return(
                <Option index={i} key={i} val={value} title={title} onSelect={this.onSelect}/>
            )
        })

        return (
            <div className="customSelect">
                {isOpen ? <div className="customOverlay" onClick={() => this.toggleSelect(false)}></div> : null}
                <div className={`currentValue ${isOpen ? 'arrowRotate' : ''}`}
                     onClick={() => this.toggleSelect()}>
                    { selected ? selected.title : defaultValue }
                </div>

                <ul className={isOpen ? '' : 'hide'}>
                    <Option isDefault={true} title={defaultValue} />
                    { optionsList }
                </ul>
            </div>
        )
    }
}

const Option = ({index, val, title, onSelect, isDefault}) => {
    return (
        <li index={index}
            val={val}
            onClick={(e) => onSelect(e, index)}
            className={isDefault ? 'defaultOption' : ''}>
            {title}
        </li>
    )
}