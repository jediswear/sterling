import React, {Component} from 'react'
import CustomSelect from '../customSelect/customSelect'
import './app.scss'

import NewsService from '../../services/newsService'

const newsService = new NewsService()

class App extends Component {

    state = {
        sources: [],
        news: [],
        openedItem: null,
        favoriteArticles: [],
        notes: [],
        showModalAddNoteId: null,
        showModalAddNote: false,
        noteText: null
    }

    componentDidMount() {
        newsService.getSources()
            .then(data => {
                this.setState({
                    sources: data.sources
                })
            })
            .catch(err => {
                console.log(err);
            })

        const notes = window.localStorage.getItem('notes')
        const favoriteArticles = window.localStorage.getItem('articles')

        this.setState({
            notes: JSON.parse(notes),
            favoriteArticles: JSON.parse(favoriteArticles)
        })
    }

    onSelect = (item) => {
        newsService.getNews(item.value)
            .then(data => {
                console.log(data);

                this.setState({
                    news: data.articles
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    accordeonItemClick = (itemIndex) => {
        this.setState({
            openedItem: itemIndex
        })
    }

    saveToLocalStorage = (articleId) => {
        if (!window.localStorage.articles) {
            window.localStorage.setItem('articles', JSON.stringify([articleId]))
        } else {
            let articles = window.localStorage.getItem('articles')
            articles = JSON.parse(articles)
            const isExist = articles.includes(articleId)

            if (!isExist) {
                articles.push(articleId)
                window.localStorage.setItem('articles', JSON.stringify(articles))

                this.setState({
                    favoriteArticles: articles
                })
            }
        }
    }

    showModalAdd = (id) => {
        this.setState(({showModalAddNote}) => {
            return {
                showModalAddNoteId: id,
                showModalAddNote: !showModalAddNote
            }
        })
    }

    noteEdit(e) {
        const val = e.target.value

        this.setState({
            noteText: val
        })
    }

    saveNote() {
        const {noteText, showModalAddNoteId: id} = this.state

        if (!window.localStorage.notes) {
            window.localStorage.setItem('notes', JSON.stringify([
                {
                    id,
                    content: [noteText]
                }
            ]))
        } else {
            let notes = window.localStorage.getItem('notes')
            notes = JSON.parse(notes)

            let isExist
            notes.forEach((item, i) => {
                isExist = item.id === id ? i : isExist
            })

            if (isExist) {
                notes[isExist].content.push(noteText)
                window.localStorage.setItem('notes', JSON.stringify(notes))
            } else {
                notes.push({
                    id,
                    content: [noteText]
                })

                window.localStorage.setItem('notes', JSON.stringify(notes))

            }

            this.setState({
                noteText: null,
                notes,
                showModalAddNote: false
            })
        }
    }

    showAllNotes = (id) => {
        const { notes } = this.state

        let res

            notes.forEach(item => {
                if (item.id === id){
                    res = item
                }
            })

        if(res){
            console.log(res.content);
        } else {
            console.log('no comments');
        }
    }


    render() {
        const {sources, news, openedItem, favoriteArticles, showModalAddNote} = this.state

        const itemsForSelect = sources.map((el) => {
            return {
                value: el.id,
                title: el.name
            }
        })

        return (
            <div>
                {showModalAddNote ?
                    <div className="modalAddNote">
                        <textarea name="" id="" cols="30" rows="10" onChange={(e) => this.noteEdit(e)}></textarea>
                        <button className="save" onClick={() => this.saveNote()}>Save</button>
                    </div>
                    : null
                }
                <CustomSelect items={itemsForSelect} eventChange={this.onSelect}/>
                <div className="acordeon">
                    {news.length > 0
                        ? news.map(({url, content, urlToImage, title}, i) => {
                                return (
                                    <AccordeonItem
                                        url={url}
                                        title={title}
                                        ind={i}
                                        key={i}
                                        urlToImage={urlToImage}
                                        content={content}
                                        isOpen={openedItem === i}
                                        isFavourite={favoriteArticles.includes(url)}
                                        showModalAdd={this.showModalAdd}
                                        showAllNotes={this.showAllNotes}
                                        accordeonItemClick={this.accordeonItemClick}
                                        saveToLocalStorage={this.saveToLocalStorage}
                                    />
                                )
                            }
                        )
                        : null
                    }
                </div>
            </div>
        )
    }
}

const AccordeonItem = ({
                           url,
                           content,
                           urlToImage,
                           title,
                           ind,
                           accordeonItemClick,
                           isOpen,
                           saveToLocalStorage,
                           isFavourite,
                           showModalAdd,
                           showAllNotes
                       }) => {
    return (
        <div className="ac-item">
            <div className="ac-head" onClick={() => accordeonItemClick(ind)}>
                {title}
                <span className="ac-controls">
                    <button onClick={() => showAllNotes(url)}><i className="material-icons">description</i></button>
                    <button onClick={() => showModalAdd(url)}><i className="material-icons">edit</i></button>
                    <button onClick={() => saveToLocalStorage(url)}><i
                        className={`material-icons ${isFavourite ? 'favourite' : ''}`}>star</i></button>
                </span>
            </div>
            <div className={`ac-body ${isOpen ? 'ac-open' : ''}`}>
                <img src={urlToImage} alt={title}/>
                <p>{content}</p>
                <a href={url}>Link</a>
            </div>
        </div>
    )
}

export default App