export default class NewsService {
    apiBase = 'https://newsapi.org/v2'
    apiKey = 'cd3f73f37ae7413c815218342da3aa99'

    getSources = async () => {
        const url = `${this.apiBase}/sources?apiKey=${this.apiKey}`

        const res = await fetch(url)

        return await res.json()
    }

    getNews = async (id) =>{
        const url = `${this.apiBase}/everything?sources=${id}&apiKey=${this.apiKey}`

        const res = await fetch(url)

        return await res.json()
    }
}